package server

import (
	"context"
	"entity"
	"fmt"

	"google.golang.org/grpc"
)

//LaunchServer : Launch
func LaunchServer() {
	ctx := context.Background()

	conn, err := grpc.Dial("localhost:5005", grpc.WithInsecure())
	handleError(err)

	client := entity.NewDisplayNameServiceClient(conn)

	request := &entity.Request{
		Classtype:    ".HashUtil",
		FunctionName: "ContentHash",
	}
	request.FunctionParameters = append(request.FunctionParameters, "Secret to Be Hashed")

	result, err := client.GetDisplayName(ctx, request)
	handleError(err)

	fmt.Println(result)
}

func handleError(err error) {
	if err != nil {
		panic(err)
	}
}
