package entity

//RPCRelayMessage : Agreed Upon slxRPC Structure
type RPCRelayMessage struct {
	ClassType          string        `json:"classtype"`
	FunctionName       string        `json:"functionName"`
	FunctionParameters []interface{} `json:"functionParameters"`
}
